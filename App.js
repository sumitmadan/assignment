/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */



import React, {useEffect, useState} from 'react';
import {SafeAreaView, Text, StyleSheet} from 'react-native';

const App = () => {

  const [clicksArray, setClicksArray] = useState([]);

  useEffect(() => {
    timestampArray();
  }, []);

  const clicks = [
    {ip: '22.22.22.22', timestamp: '3/11/2016 02:02:58', amount: 7.0},
    {ip: '11.11.11.11', timestamp: '3/11/2016 02:12:32', amount: 6.5},
    {ip: '11.11.11.11', timestamp: '3/11/2016 02:13:11', amount: 7.25},
    {ip: '44.44.44.44', timestamp: '3/11/2016 02:13:54', amount: 8.75},
    {ip: '22.22.22.22', timestamp: '3/11/2016 05:02:45', amount: 11.0},
    {ip: '44.44.44.44', timestamp: '3/11/2016 06:32:42', amount: 5.0},
    {ip: '22.22.22.22', timestamp: '3/11/2016 06:35:12', amount: 2.0},
    {ip: '11.11.11.11', timestamp: '3/11/2016 06:45:01', amount: 12.0},
    {ip: '11.11.11.11', timestamp: '3/11/2016 06:59:59', amount: 11.75},
    {ip: '22.22.22.22', timestamp: '3/11/2016 07:01:53', amount: 1.0},
    {ip: '11.11.11.11', timestamp: '3/11/2016 07:02:54', amount: 4.5},
    {ip: '33.33.33.33', timestamp: '3/11/2016 07:02:54', amount: 15.75},
    {ip: '66.66.66.66', timestamp: '3/11/2016 07:02:54', amount: 14.25},
    {ip: '22.22.22.22', timestamp: '3/11/2016 07:03:15', amount: 12.0},
    {ip: '22.22.22.22', timestamp: '3/11/2016 08:02:22', amount: 3.0},
    {ip: '22.22.22.22', timestamp: '3/11/2016 09:41:50', amount: 4.0},
    {ip: '22.22.22.22', timestamp: '3/11/2016 10:02:54', amount: 5.0},
    {ip: '22.22.22.22', timestamp: '3/11/2016 11:05:35', amount: 10.0},
    {ip: '22.22.22.22', timestamp: '3/11/2016 13:02:21', amount: 6.0},
    {ip: '55.55.55.55', timestamp: '3/11/2016 13:02:40', amount: 8.0},
    {ip: '44.44.44.44', timestamp: '3/11/2016 13:02:55', amount: 8.0},
    {ip: '55.55.55.55', timestamp: '3/11/2016 13:33:34', amount: 8.0},
    {ip: '55.55.55.55', timestamp: '3/11/2016 13:42:24', amount: 8.0},
    {ip: '55.55.55.55', timestamp: '3/11/2016 13:47:44', amount: 6.25},
    {ip: '55.55.55.55', timestamp: '3/11/2016 14:02:54', amount: 4.25},
    {ip: '55.55.55.55', timestamp: '3/11/2016 14:03:04', amount: 5.25},
    {ip: '55.55.55.55', timestamp: '3/11/2016 15:12:55', amount: 6.25},
    {ip: '22.22.22.22', timestamp: '3/11/2016 16:02:36', amount: 8.0},
    {ip: '55.55.55.55', timestamp: '3/11/2016 16:22:11', amount: 8.5},
    {ip: '55.55.55.55', timestamp: '3/11/2016 17:18:19', amount: 11.25},
    {ip: '55.55.55.55', timestamp: '3/11/2016 18:19:20', amount: 9.0},
    {ip: '22.22.22.22', timestamp: '3/11/2016 23:59:59', amount: 9.0}
  ];
  

  // Array for all timestamp in hours.
  const timestampArray = () => {
    const uniqueTimestampHours = [
      ...new Set(
        clicks.map((item) => {
          return new Date(Date.parse(item.timestamp)).getHours();
        })
      )
    ];
    /* output for uniqueTimestampHours
    uniqueTimestampHours [2, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 18, 23] */

    const arrayOfIP = clicks.map((item) => item.ip);
    /* output for arrayOfIP
    arrayOfIP ["22.22.22.22", "11.11.11.11", "11.11.11.11", "44.44.44.44", "22.22.22.22", "44.44.44.44", "22.22.22.22", "11.11.11.11", "11.11.11.11", "22.22.22.22", "11.11.11.11", "33.33.33.33", "66.66.66.66", "22.22.22.22", "22.22.22.22", "22.22.22.22", "22.22.22.22", "22.22.22.22", "22.22.22.22", "55.55.55.55", "44.44.44.44", "55.55.55.55", "55.55.55.55", "55.55.55.55", "55.55.55.55", "55.55.55.55", "55.55.55.55", "22.22.22.22", "55.55.55.55", "55.55.55.55", "55.55.55.55", "22.22.22.22"] */

    const ipFrequencyObject = arrayOfIP.reduce(function (acc, curr) {
      if (typeof acc[curr] === 'undefined') {
        acc[curr] = 1;
      } else {
        acc[curr] += 1;
      }
      return acc;
    }, {});
    /* output for ipFrequencyObject
     ipFrequencyObject {"11.11.11.11": 5, "22.22.22.22": 12, "33.33.33.33": 1, "44.44.44.44": 3, "55.55.55.55": 10, "66.66.66.66": 1} */

    const maxClicksArray = [];
    // const maxClicksObj = {};
    for (let hoursCount = 0; hoursCount < uniqueTimestampHours.length; hoursCount++) {
      let maxValue = 0;
      const maxClicksObj = {};
      for (let clickCount = 0; clickCount < clicks.length; clickCount++) {
        // const currentHoursArray = clicks[clickCount].timestamp.split(' ');
        const currentHours = new Date(Date.parse(clicks[clickCount].timestamp)).getHours();
        const ip = clicks[clickCount].ip.toString();
        if (uniqueTimestampHours[hoursCount] === currentHours && ipFrequencyObject[ip] <= 10) {
          if (clicks[clickCount].amount > maxValue) {
            maxValue = clicks[clickCount].amount;
            maxClicksObj.ip = clicks[clickCount].ip;
            maxClicksObj.timestamp = clicks[clickCount].timestamp;
            maxClicksObj.amount = maxValue;
          }
        }
      }
      // console.log('maxClicksObj', maxClicksObj);
      if (Object.keys(maxClicksObj).length > 0) {
        maxClicksArray.push(maxClicksObj);
      }
    }
    setClicksArray(maxClicksArray);
  };
  /* output for maxClicksArray
  [
    {
      "amount": 8.75,
      "ip": "44.44.44.44",
      "timestamp": "3/11/2016 02:13:54"
    },
    {
      "amount": 12,
      "ip": "11.11.11.11",
      "timestamp": "3/11/2016 06:45:01"
    },
    {
      "amount": 15.75,
      "ip": "33.33.33.33",
      "timestamp": "3/11/2016 07:02:54"
    },
    {
      "amount": 8,
      "ip": "55.55.55.55",
      "timestamp": "3/11/2016 13:02:40"
    },
    {
      "amount": 5.25,
      "ip": "55.55.55.55",
      "timestamp": "3/11/2016 14:03:04"
    },
    {
      "amount": 6.25,
      "ip": "55.55.55.55",
      "timestamp": "3/11/2016 15:12:55"
    },
    {
      "amount": 8.5,
      "ip": "55.55.55.55",
      "timestamp": "3/11/2016 16:22:11"
    },
    {
      "amount": 11.25,
      "ip": "55.55.55.55",
      "timestamp": "3/11/2016 17:18:19"
    },
    {
      "amount": 9,
      "ip": "55.55.55.55",
      "timestamp": "3/11/2016 18:19:20"
    }
  ] */
  return (
    
      <SafeAreaView style={styles.container}>
      {clicksArray.map((item, index) => (
        <Text key={index}>
          {': {'}
          ip: "{item.ip}", timestamp: "{item.timestamp}", amount: "{item.amount}"{'}'}
        </Text>
      ))}
    </SafeAreaView>
   
  );
};

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    
    marginHorizontal: 10
  },
  rowContainer: {
    flexDirection: 'row'
  }
  
});

export default App;
